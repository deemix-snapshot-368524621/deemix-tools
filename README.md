# deemix-tools
These are some usefull tools to install deemix-pyweb if you're not tech savvy

### Windows
## Install script
1. Download `install_windows.bat` file to your PC and place it in the folder where you want Deemix to live<br>
2. Start the `install_windows.bat` as administrator<br>
3. Wait for it to finish, then run the `start.bat`<br>

### MacOS
1. Download `install_mac.sh` file to your Mac and place it in the folder where you want Deemix to live<br>
2. Open a terminal and type `sh` and a space, then drag `install_mac.sh` into your terminal window and press `enter`<br>
3. The web-gui will launch when the installer is finished, and launching it in the future can be done with the generated file `startdeemix.command`<br>
4. To update to the latest build, run the generated file `updatedeemix.command`<br>

### Debian/Ubuntu
1. Download `install_debian.sh` file to your PC and place it in the folder where you want Deemix to live<br>
2. Open a terminal and type ./install_debian.sh and press `enter`<br>
3. The pyweb GUI will launch when the installer is finished, and launching it in the future can be done with the generated file `startdeemix.sh`<br>
4. To update to the latest build, run the generated file `updatedeemix.sh`<br>

# License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
