#!/bin/sh
if [[ "$(which python3)" -eq "" ]]
then
	echo "python3 is not installed!"
	while true; do
    read -p "Do you wish to install python3? [y/N]" yn
    case $yn in
        [Yy]* ) /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"; brew install python3; break;;
        [Nn]* ) exit;;
        * ) exit;;
    esac
	done
fi

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "${DIR}"

curl https://git.rip/RemixDev/deemix-pyweb/-/archive/main/deemix-pyweb-main.zip -o deemix.zip
unzip deemix.zip
rm deemix.zip
mv deemix-pyweb-main deemix
cd deemix

rm -rf webui
curl https://git.rip/RemixDev/deemix-webui/-/archive/main/deemix-webui-main.zip -o webui.zip
unzip webui.zip
rm webui.zip
mv deemix-webui-main webui

majorVersion=$(sw_vers -productVersion | cut -d '.' -f 1)
minorVersion=$(sw_vers -productVersion | cut -d '.' -f 2)

cd "${DIR}"
file="startdeemix.command"
echo "DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" > $file
echo "cd "${DIR}"" >> $file
echo "cd deemix" >> $file
if [[ "$majorVersion" -eq "10" && (( "$minorVersion" > "13" )) ]]
then
	echo "python3 deemix-pyweb.py" >> $file
else
	echo "open http://127.0.0.1:6595" >> $file
	echo "python3 server.py" >> $file
fi
cat $file
chmod a+x startdeemix.command

file="updatedeemix.command"
echo "DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" > $file
echo "cd "${DIR}"" >> $file
echo "rm -rf deemix" >> $file
echo "curl https://git.rip/RemixDev/deemix-pyweb/-/archive/main/deemix-pyweb-main.zip -o deemix.zip" >> $file
echo "unzip -o deemix.zip" >> $file
echo "rm deemix.zip" >> $file
echo "mv deemix-pyweb-main deemix" >> $file
echo "cd deemix" >> $file
echo "rm -rf webui" >> $file
echo "curl https://git.rip/RemixDev/deemix-webui/-/archive/main/deemix-webui-main.zip -o webui.zip" >> $file
echo "unzip webui.zip" >> $file
echo "rm webui.zip" >> $file
echo "mv deemix-webui-main webui" >> $file
echo "python3 -m pip install -U pip" >> $file
if [[ "$majorVersion" -eq "10" && (( "$minorVersion" > "13" )) ]]
then
	echo "python3 -m pip install -U -r requirements.txt" >> $file
	echo "echo "Deemix Updated"" >> $file
	echo "python3 deemix-pyweb.py" >> $file
else
	echo "python3 -m pip install -U -r server-requirements.txt" >> $file
	echo "echo "Deemix Updated"" >> $file
	echo "open http://127.0.0.1:6595" >> $file
	echo "python3 server.py" >> $file
fi
cat $file
chmod a+x updatedeemix.command

cd deemix
python3 -m pip install -U pip
if [[ "$majorVersion" -eq "10" && (( "$minorVersion" > "13" )) ]]
then
	python3 -m pip install -U -r requirements.txt
	echo "Deemix installed :)"
	python3 deemix-pyweb.py
else
	python3 -m pip install -U -r server-requirements.txt
	echo "Deemix installed :)"
	open http://127.0.0.1:6595
	python3 server.py
fi
